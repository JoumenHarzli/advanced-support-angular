import { Component } from '@angular/core';

@Component({
  selector: 'app-cookapp',
  template: `<app-todo></app-todo>`,
  styleUrls: ['app.component.scss']
})
export class AppComponent {

}
