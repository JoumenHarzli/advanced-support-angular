import { Todo } from '../models/todo.model';

export class TodoActions {
    /* Loading actions*/
    static TODOS_LOAD_REQUEST = 'TODO_LOAD_REQUEST';
    static TODOS_LOAD_FAIL = 'TODO_LOAD_FAIL';
    static TODOS_LOAD_SUCCESS = 'TODO_LOAD_SUCCESS';

    /* Adding Actions */
    static TODOS_ADD_REQUEST = 'TODO_ADD_REQUEST';
    static TODOS_ADD_FAIL = 'TODO_ADD_FAIL';
    static TODOS_ADD_SUCCESS = 'TODO_ADD_SUCCESS';

    /* Editing Actions */
    static TODOS_EDIT_REQUEST = 'TODO_EDIT_REQUEST';
    static TODOS_EDIT_FAIL = 'TODO_EDIT_FAIL';
    static TODOS_EDIT_SUCCESS = 'TODO_EDIT_SUCCESS';

    /* Deleting Actions */
    static TODOS_DELETE_REQUEST = 'TODO_DELETE_REQUEST';
    static TODOS_DELETE_FAIL = 'TODO_DELETE_FAIL';
    static TODOS_DELETE_SUCCESS = 'TODO_DELETE_SUCCESS';

    /* Select Action */
    static TODOS_SELECT = 'TODO_SELECT';

    /**
     * Loading actions defintion
     */
    loadTodosRequest = () => {
        return {
            type: TodoActions.TODOS_LOAD_REQUEST
        };
    }

    loadTodosSuccess = (todos: Todo[]) => {
        return {
            type: TodoActions.TODOS_LOAD_SUCCESS,
            payload: todos
        };
    }

    loadTodosFail = (error) => {
        return {
            type: TodoActions.TODOS_LOAD_FAIL,
            payload: error
        };
    }

    /**
     * Adding actions defintion
     */
    addTodoRequest = (todo: Todo) => {
        return {
            type: TodoActions.TODOS_ADD_REQUEST,
            payload: todo
        };
    }

    addTodoSuccess = (todo: Todo) => {
        return {
            type: TodoActions.TODOS_ADD_SUCCESS,
            payload: todo
        };
    }

    addTodoFail = (error) => {
        return {
            type: TodoActions.TODOS_ADD_FAIL,
            payload: error
        };
    }

    /**
     * Editing actions defintion
     */
    editTodoRequest = (todo: Todo) => {
        return {
            type: TodoActions.TODOS_EDIT_REQUEST,
            payload: todo
        };
    }

    editTodoSuccess = (todo: Todo) => {
        return {
            type: TodoActions.TODOS_EDIT_SUCCESS,
            payload: todo
        };
    }

    editTodoFail = (error) => {
        return {
            type: TodoActions.TODOS_EDIT_FAIL,
            payload: error
        };
    }

    /**
     * Deleteing actions defintion
     */
    deleteTodoRequest = (todo: Todo) => {
        return {
            type: TodoActions.TODOS_DELETE_REQUEST,
            payload: todo
        };
    }

    deleteTodoSuccess = (todo: Todo) => {
        return {
            type: TodoActions.TODOS_DELETE_SUCCESS,
            payload: todo
        };
    }

    deleteTodoFail = (error) => {
        return {
            type: TodoActions.TODOS_DELETE_FAIL,
            payload: error
        };
    }

    /**
     * Select Action
     */
    selectTodo = (id: number) => {
        return {
            type: TodoActions.TODOS_SELECT,
            payload: id
        };
    }
}
