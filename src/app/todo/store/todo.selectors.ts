export const getEntities = (state) => state.get('entities').toJS();
export const getTodos = (state) => state.getIn(['entities', 'todos']).toJS();
export const getIds = (state) => state.get('ids').toJS();

export const getSelectedId = (state) => state.get('selectedId');
export const getError = (state) => state.get('error');
export const getLoading = (state) => state.get('loading');
