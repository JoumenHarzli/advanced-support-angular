import { Action } from '@ngrx/store';
import { fromJS, Map, List } from 'immutable';
import { normalize, denormalize } from 'normalizr';
import { union } from 'lodash';
import { Todo, todosSchema, todosListSchema } from '../models/todo.model';
import { TodoActions } from './todo.actions';

export const initialState = fromJS({
    loading: false,
    ids: [],
    entities: {
        todos: {},
    },
    error: '',
    selectedId: 0
});

export interface State {
    loading: boolean;
    ids: List<Number>;
    entities: {
        todos: Map<Number, Todo>;
    };
    error: string;
    selectedId: number;
}

export const reducer = (state = initialState, action: Action): State => {
    switch (action.type) {

        case TodoActions.TODOS_ADD_REQUEST:
        case TodoActions.TODOS_EDIT_REQUEST:
        case TodoActions.TODOS_DELETE_REQUEST:
        case TodoActions.TODOS_LOAD_REQUEST:
            return state.withMutations(
                (_state) => {
                    _state.set('error', '')
                        .set('loading', true);
                });

        case TodoActions.TODOS_ADD_FAIL:
        case TodoActions.TODOS_EDIT_FAIL:
        case TodoActions.TODOS_DELETE_FAIL:
        case TodoActions.TODOS_LOAD_FAIL:
            return state.withMutations(
                (_state) => {
                    _state.set('error', action.payload)
                        .set('loading', false);
                });

        case TodoActions.TODOS_EDIT_SUCCESS:
        case TodoActions.TODOS_ADD_SUCCESS:
            return state.withMutations(
                (_state) => {
                    const idTodo = action.payload.id;
                    _state.set('loading', false);
                    _state.setIn(['entities', 'todos', idTodo.toString()], action.payload)
                        .update('ids', list => list.push(idTodo));
                });

        case TodoActions.TODOS_DELETE_SUCCESS:
            return state.withMutations(
                (_state) => {
                    const idTodo = action.payload.id;
                    const pos = _state.get('ids').indexOf(idTodo);
                    _state.set('loading', false);
                    const todo = action.payload;
                    _state.deleteIn(['entities', 'todos', idTodo.toString()]);
                    _state.deleteIn(['ids', pos]);
                });

        case TodoActions.TODOS_LOAD_SUCCESS:
            const normalizedEntities = normalize(action.payload, todosListSchema);
            return state.withMutations(
                (_state) => {
                    const mergedIds = union(_state.get('ids').toJS(), normalizedEntities.result);
                    _state.set('loading', false)
                        /*Object.keys(normalizedEntities.entities).forEach((value, key) => {
                            _state.setIn(['entities', value], Map(normalizedEntities.entities[value]));
                        });*/
                        .setIn(['entities', 'todos'], Map(normalizedEntities.entities.todos))
                        .set('ids', List(mergedIds));
                });

        case TodoActions.TODOS_SELECT:
            return state.set('selectedId', action.payload);

        default:
            return state;
    }
};
