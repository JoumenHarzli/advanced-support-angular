import { Injectable } from '@angular/core';
import { Action } from '@ngrx/store';
import { Effect, Actions } from '@ngrx/effects';
import { Observable } from 'rxjs/Observable';

import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { of } from 'rxjs/observable/of';

import { TodoService } from '../services/todo.service';
import { TodoActions } from './todo.actions';
import { Todo } from '../models/todo.model';

@Injectable()
export class TodoEffects {

    @Effect() addTodo$ = this._actions$
        .ofType(TodoActions.TODOS_ADD_REQUEST)
        .map((action) => action.payload)
        .mergeMap((todo) => this._todoService.saveTodo(todo))
        .map((todo) => this._todoActions.addTodoSuccess(todo))
        .catch(err => of(this._todoActions.addTodoFail(err)));

    @Effect() editTodo$ = this._actions$
        .ofType(TodoActions.TODOS_EDIT_REQUEST)
        .map((action) => action.payload)
        .mergeMap((todo) => this._todoService.saveTodo(todo))
        .map((todo) => this._todoActions.editTodoSuccess(todo))
        .catch(err => of(this._todoActions.editTodoFail(err)));

    @Effect() deleteTodo$ = this._actions$
        .ofType(TodoActions.TODOS_DELETE_REQUEST)
        .map((action) => action.payload)
        .mergeMap((todo) => this._todoService.deleteTodo(todo))
        .map((todo) => this._todoActions.deleteTodoSuccess(todo))
        .catch(err => of(this._todoActions.deleteTodoFail(err)));

    @Effect() loadTodos$ = this._actions$
        .ofType(TodoActions.TODOS_LOAD_REQUEST)
        .switchMap(() => this._todoService.loadTodos())
        .map((todos) => this._todoActions.loadTodosSuccess(todos))
        .catch(err => of(this._todoActions.loadTodosFail(err)));

    constructor(
        private _actions$: Actions,
        private _todoService: TodoService,
        private _todoActions: TodoActions) { }

}
