import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { EffectsModule } from '@ngrx/effects';

import { TodoComponent } from './containers/todo.component';
import { ListTodoComponent } from './components/listtodo.component';
import { FormTodoComponent } from './components/formtodo.component';
import { TodoService } from './services/todo.service';
import { TodoEffects } from './store/todo.effects';
import { TodoActions } from './store/todo.actions';

@NgModule({
    imports: [CommonModule, FormsModule, HttpModule, ReactiveFormsModule,
        EffectsModule.run(TodoEffects)],
    declarations: [TodoComponent, ListTodoComponent, FormTodoComponent],
    exports: [TodoComponent],
    providers: [TodoService, TodoActions]
})

export class TodoModule {
}
export { reducer } from './store/todo.reducer';
