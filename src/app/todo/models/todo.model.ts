import { schema } from 'normalizr';
import { Record } from 'immutable';

export const TodoRecord = Record({
    userId: 0,
    id: 0,
    title: null,
    completed: null
});

export interface ITodo {
    userId?: number;
    id?: number;
    title?: string;
    completed?: boolean;
}

export class Todo extends TodoRecord {
    userId: number;
    id: number;
    title: string;
    completed: boolean;

    constructor(config) {
        super(config);
    }
}

export const todosSchema = new schema.Entity('todos');
export const todosListSchema = new schema.Array(todosSchema);
