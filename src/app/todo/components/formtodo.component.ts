import { Component, Input, Output, ChangeDetectionStrategy, OnInit, EventEmitter, OnChanges } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';

import { Todo } from '../models/todo.model';

@Component({
    selector: 'app-formtodo',
    template: `
    <div>
        <form novalidate [formGroup]="todoForm" (ngSubmit)="onSubmit()">
            <div>
                <label for="_title">Title:</label>
                <input type="text" id="_title" formControlName="title" />
                <div *ngIf="formErrors.title.length > 0">
                    <div *ngFor="let error of formErrors.title">
                        <div>{{error}}</div>
                    </div>
                </div>
            </div>
            <div>
                <label for="_completed">Completed:</label>
                <input type="checkbox" id="_completed" formControlName="completed" />
            </div>
            <div *ngIf="listUsers">
                <label for="_user">Users:</label>
                <select id="_user" formControlName="userId">
                    <option *ngFor="let user of listUsers ; let i = index" [selected]="i === 0" [value]="user.id">{{ user.name }}</option>
                </select>
                <div *ngIf="formErrors.userId.length > 0">
                    <div *ngFor="let error of formErrors.userId">
                        <div>{{error}}</div>
                    </div>
                </div>
            </div>
            <input type="submit" value="Save" [disabled]="!todoForm.valid"/>
        </form>
    </div>`,
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class FormTodoComponent implements OnInit, OnChanges {
    @Input()
    todo: Todo;

    @Input()
    listUsers: Object[];

    @Output()
    saveTodoEvent = new EventEmitter();

    todoForm: FormGroup;

    formErrors = {
        'title': [''],
        'userId': ['']
    };

    validationMessages = {
        'title': {
            'required': 'name is required'
        },
        'userId': {
            'validUser': 'Unvalid user'
        }
    };

    constructor(private _formBuilder: FormBuilder) {
    }

    ngOnInit() {}

    ngOnChanges() {
        this.buildForm();
    }

    buildForm = () => {
        this.todoForm = this._formBuilder.group({
            id: [this.todo.id],
            title: [this.todo.title, Validators.required],
            completed: [this.todo.completed], // don't put Validators.required
            userId: [this.todo.userId, this.validateUserId]
        });
        this.todoForm.valueChanges.subscribe((data) => this.onValueChanged(data));
        this.onValueChanged();
    }

    onValueChanged(data?: any) {
        if (!this.todoForm) { return; }
        const form = this.todoForm;

        for (const field in this.formErrors) {
            this.formErrors[field] = [''];
            const control = form.get(field);

            if (control && control.dirty && !control.valid) {
                const messages = this.validationMessages[field];
                for (const key in control.errors) {
                    this.formErrors[field].push(messages[key]);
                }
            }
        }
    }

    validateUserId = (c: FormControl) => {
        return (c.value > 0) ? null : { validUser: false };
    }

    onSubmit = () => {
        if (this.todoForm.valid) {
            this.saveTodoEvent.emit(this.todoForm.value);
        }
    }
}
