import { Component, Input, ChangeDetectionStrategy, Output, EventEmitter } from '@angular/core';

import { Todo } from '../models/todo.model';

@Component({
    selector: 'app-listtodo',
    template: `
    <div>
            <div *ngFor="let todo of todos">
                <div>{{todo.title}}</div>
                <div>{{todo.completed ? 'completed' : 'not completed'}}</div>
                <input type="button" (click)="deleteTodoEvent.emit(todo)" value="Delete">
                <input type="button" (click)="editTodoEvent.emit(todo)" value="Edit">
            </div>
    </div>`,
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class ListTodoComponent {
    @Input()
    todos: Todo[];

    @Output()
    editTodoEvent = new EventEmitter();

    @Output()
    deleteTodoEvent = new EventEmitter();

    constructor() {
    }
}
