import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { from } from 'rxjs/observable/from';
import 'rxjs/add/operator/map';

import { Todo } from '../models/todo.model';
import { TODO_API } from '../../common/config';

@Injectable()
export class TodoService {

    constructor(private _http: Http) {
    }

    addTodo(todo: Todo): Observable<Todo> {
        return this._http.post(TODO_API, todo)
            .map((response: Response) => todo.id = response.json());
    }

    editTodo(todo: Todo): Observable<Todo> {
        return this._http.put(TODO_API + `/${todo.id}`, todo)
            .map(() => todo);
    }

    saveTodo = (todo: Todo): Observable<Todo> => {
        if (todo.id === 0) {
            return this.addTodo(todo);
        } else {
            return this.editTodo(todo);
        }
    }

    deleteTodo(todo: Todo): Observable<Todo> {
        return this._http.delete(TODO_API + `/${todo.id}`)
            .map(() => todo);
    }

    loadTodos = (): Observable<Todo[]> => {
        return this._http.get(TODO_API)
            .map((response: Response) => response.json());
    }

    loadUsers = () => {
        return this._http.get('https://jsonplaceholder.typicode.com/users')
            .map((response: Response) => response.json());
    }
}
