import { Component, OnInit, ChangeDetectionStrategy, OnDestroy } from '@angular/core';

import { Store } from '@ngrx/store';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toArray';
import { denormalize } from 'normalizr';

import { State } from '../store/todo.reducer';
import { TodoActions } from '../store/todo.actions';
import { TodoService } from '../services/todo.service';
import { Todo, todosSchema, todosListSchema } from '../models/todo.model';
import * as Selectors from '../store/todo.selectors';

@Component({
    selector: 'app-todo',
    template: `
    <div *ngIf="(loading$ | async)">Loading please wait ...</div>
    <div *ngIf="(error$ | async)">Error : {{error$|async}}</div>
    <app-formtodo [todo]="selectedTodo$ | async" [listUsers]="listUsers" (saveTodoEvent)="saveTodo($event)"></app-formtodo>
    <button (click)="newTodo()">Add New</button>
    <app-listtodo [todos]="todos$ | async" (editTodoEvent)="editTodo($event)" (deleteTodoEvent)="deleteTodo($event)"></app-listtodo>
    `
})
export class TodoComponent implements OnInit {
    loading$: Observable<boolean>;
    todos$: Observable<Todo[]>;
    selectedTodo$: Observable<Todo>;
    error$: Observable<String>;
    listUsers: Object[];

    constructor(private _store: Store<State>, private _todoActions: TodoActions,
        private _todoService: TodoService) { }

    ngOnInit() {
        this._todoService.loadUsers().subscribe((users) => this.listUsers = users);
        this.subscribeToStore();
        /* Get users */
        this.loadTodos();
    }

    subscribeToStore() {
        this.todos$ = this._store.map(
            (storeState) => {
                return denormalize(Selectors.getIds(storeState), todosListSchema, Selectors.getEntities(storeState));
            }
        );

        this.selectedTodo$ = this._store.map(
            (storeState) => {
                const id = Selectors.getSelectedId(storeState);
                if (id === 0) {
                    return new Todo({ id: 0 });
                }
                const todo = denormalize([id], todosListSchema, Selectors.getEntities(storeState))[0];
                return todo;
            }
        );

        this.error$ = this._store.map(
            (storeState) => {
                return Selectors.getError(storeState);
            }
        );

        this.loading$ = this._store.map(
            (storeState) => {
                return Selectors.getLoading(storeState);
            }
        );
    }

    newTodo() {
        this._store.dispatch(this._todoActions.selectTodo(0));
    }

    editTodo(_todo) {
        this._store.dispatch(this._todoActions.selectTodo(_todo.id));
    }

    saveTodo(_todo) {
        if (_todo.id === 0) {
            this._store.dispatch(this._todoActions.addTodoRequest(_todo));
        } else {
            this._store.dispatch(this._todoActions.editTodoRequest(_todo));
        }
    }

    deleteTodo(_todo) {
        this._store.dispatch(this._todoActions.deleteTodoRequest(_todo));
    }

    loadTodos() {
        this._store.dispatch(this._todoActions.loadTodosRequest());
    }
}
